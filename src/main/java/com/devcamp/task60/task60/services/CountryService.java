package com.devcamp.task60.task60.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task60.task60.model.Country;
import com.devcamp.task60.task60.repository.CountryRepository;

@Service
public class CountryService {
    @Autowired
    private CountryRepository repository;
    
    public List<Country> getAllCountries(){
        return repository.findAll();
    }

    public Country getCountryByCountryCode(String countryCode){
        return repository.findByCountryCode(countryCode);
    }
}
