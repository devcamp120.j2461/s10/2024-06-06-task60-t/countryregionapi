package com.devcamp.task60.task60;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task60Application {

	public static void main(String[] args) {
		SpringApplication.run(Task60Application.class, args);
	}

}
