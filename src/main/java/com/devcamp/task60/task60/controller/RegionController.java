package com.devcamp.task60.task60.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task60.task60.model.Country;
import com.devcamp.task60.task60.model.Region;
import com.devcamp.task60.task60.services.CountryService;

@RestController
@RequestMapping("/api")
public class RegionController {
    
    @Autowired
    private CountryService countryService;

    @GetMapping("/regions")
    public ResponseEntity<List<Region>> getRegionByCountryCode(@RequestParam(name = "countryCode") String countryCode){
        
        Country country = countryService.getCountryByCountryCode(countryCode);
        if (country!=null){
            return new ResponseEntity(country.getRegions(),HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
