package com.devcamp.task60.task60.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task60.task60.model.Country;
import com.devcamp.task60.task60.services.CountryService;


@RestController
@RequestMapping("/api")
public class CountryController {
    
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")    
    public ResponseEntity<List<Country>> getAllCountries(){
        try {
            return new ResponseEntity<List<Country>>(countryService.getAllCountries(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
