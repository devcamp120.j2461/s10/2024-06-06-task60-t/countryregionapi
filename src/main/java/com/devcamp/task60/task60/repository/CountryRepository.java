package com.devcamp.task60.task60.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.task60.task60.model.Country;


@Repository
public interface CountryRepository extends JpaRepository<Country,Long> {
    Country findByCountryCode(String countryCode);
}
